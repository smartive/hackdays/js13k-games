FROM node:10-alpine as deps

WORKDIR /build

COPY ./christoph ./christoph
COPY ./doeme ./doeme
COPY ./kim ./kim
COPY ./marco ./marco
COPY ./nicola ./nicola

RUN cd christoph && npm ci
RUN cd doeme && npm ci
RUN cd kim && npm ci
RUN cd marco && npm ci
RUN cd nicola && npm ci

FROM deps as sites

RUN cd christoph && npm run build -- --output-public-path /2018/christoph/ && rm -rf ./build/main*
RUN cd doeme && npm run build -- --output-public-path /2018/doeme/ && rm -rf ./build/main*
RUN cd kim && npm run build -- --output-public-path /2018/kim/ && rm -rf ./build/main*
RUN cd marco && npm run build -- --output-public-path /2018/marco/ && rm -rf ./build/main*
RUN cd nicola && npm run build -- --output-public-path /2018/nicola/ && rm -rf ./build/main*

FROM deps as zips

RUN cd christoph && npm run package
RUN cd doeme && npm run package
RUN cd kim && npm run package
RUN cd marco && npm run package
RUN cd nicola && npm run package

FROM nginx:1.15-alpine

WORKDIR /usr/share/nginx/html/

COPY ./index.html ./2018/index.html

COPY --from=sites /build/christoph/build ./2018/christoph
COPY --from=sites /build/doeme/build ./2018/doeme
COPY --from=sites /build/kim/build ./2018/kim
COPY --from=sites /build/marco/build ./2018/marco
COPY --from=sites /build/nicola/build ./2018/nicola

COPY --from=zips /build/christoph/out/build.zip ./2018/christoph/js13k-sokoban.zip
COPY --from=zips /build/doeme/out/build.zip ./2018/doeme/doeme-flip.zip
COPY --from=zips /build/kim/out/build.zip ./2018/kim/game.zip
COPY --from=zips /build/marco/out/build.zip ./2018/marco/tron.zip
COPY --from=zips /build/nicola/out/build.zip ./2018/nicola/the-meaning-of-life.zip
