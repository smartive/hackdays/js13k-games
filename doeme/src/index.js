import './styles/main.css';
import 'kontra/dist/core';
import 'kontra/dist/gameLoop';
import 'kontra/dist/keyboard';
import 'kontra/dist/sprite';

const canvas = document.getElementById("c");
canvas.width = document.body.clientWidth;
canvas.height = document.body.clientHeight;

kontra.init();

const random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min || min;

const basePlayerConfig = {
  y: 80,
  width: 20,
  height: random(50, 150),
  fullGrown: false,
  fullSpeed: false,
};

const player = kontra.sprite({
  ...basePlayerConfig,
  x: 50,
  color: 'blue',
  dx: 2,
});

const player2 = kontra.sprite({
  ...basePlayerConfig,
  x: canvas.width - 50,
  color: 'red',
  dx: -2,
});

const obstacles = Array.from(Array(3), (_, i) => {
  const height = random(50, 150);
  const x = random(100 * i, kontra.canvas.width - 100);
  const y = random(100, kontra.canvas.height - x - height);
  const init = {
    x,
    y,
    color: 'black',
    width: 10,
    height,
    dy: random(-1, 1),
    isBottom: false,
  };

  return kontra.sprite({
    ...init,
    init,
  });
});

const RANDOM_GROW_FACTOR_1 = Math.random() * 10;
const RANDOM_GROW_FACTOR_2 = Math.random() * 10;

const MAX_PLAYER_HEIGHT = 100;
const MIN_PLAYER_HEIGHT = 10;
const grow = (sprite, factor) => {
  const { height, fullGrown } = sprite;
  sprite.fullGrown = height >= MAX_PLAYER_HEIGHT ? true : height <= MIN_PLAYER_HEIGHT ? false : fullGrown;

  if (height < MAX_PLAYER_HEIGHT && !fullGrown) {
    sprite.height += factor * 20;
  } else if (height > MIN_PLAYER_HEIGHT) {
    sprite.height -= factor * 20;
  }
};

const MAX_SPEED = 15;
const MIN_SPEED = 2;
const moveY = (sprite) => {
  const { dy, y, isBottom } = sprite;
  const MIN_Y = 0;
  const MAX_Y = kontra.canvas.height - sprite.height;

  sprite.isBottom = y >= MAX_Y ? true : y <= MIN_Y ? false : isBottom;
  const maxSpeed = sprite.isBottom ? -MIN_SPEED : MAX_SPEED;
  const minSpeed = sprite.isBottom ? -MAX_SPEED : MIN_SPEED;

  if (dy < maxSpeed && !isBottom) {
    sprite.dy += 0.2;
  } else if (dy > minSpeed) {
    sprite.dy -= 0.2;
  }
};

const speed = (sprite, factor, negative) => {
  const { dx, fullSpeed } = sprite;
  const maxSpeed = negative ? -MIN_SPEED : MAX_SPEED;
  const minSpeed = negative ? -MAX_SPEED : MIN_SPEED;

  sprite.fullSpeed = dx >= maxSpeed ? true : dx <= minSpeed ? false : fullSpeed;

  if (dx < maxSpeed && !fullSpeed) {
    sprite.dx += factor;
  } else if (dx > minSpeed) {
    sprite.dx -= factor;
  }
};

const interact = (sprite, up, down) => {
  const MIN_Y = 0;
  const MAX_Y = kontra.canvas.height - sprite.height;

  if (up && sprite.dy >= -10 && sprite.y > MIN_Y) {
    sprite.dy -= 2;
  }

  if (down && sprite.dy <= 10 && sprite.y < MAX_Y) {
    sprite.dy += 2;
  }

  if (sprite.y < MIN_Y && sprite.dy < 0) {
    sprite.y = sprite.dy = 0;
  }

  if (sprite.y > MAX_Y && sprite.dy > 0) {
    sprite.dy = 0;
    sprite.y = MAX_Y;
  }
};

let highscore = 0;
const game = kontra.gameLoop({
  update: (dt) => {
    const {
      canvas: { width },
      keys: { pressed },
    } = kontra;

    player2.update();
    player.update();
    obstacles.forEach((obstacle) => obstacle.update());

    const collison =
      player.collidesWith(player2) || obstacles.some((o) => o.collidesWith(player) || o.collidesWith(player2));
    if (pressed('r') && collison) {
      player.dx = 2;
      player2.dx = -2;
      obstacles.forEach((obstacle, i) => {
        obstacle.dy = obstacle.init.dy;
        obstacle.x = random(100 * i, kontra.canvas.width - 100);
        obstacle.y = obstacle.init.y;
      });
      player.x = 50;
      player.y = basePlayerConfig.y;
      player.height = basePlayerConfig.height;
      player2.x = canvas.width - 50,
      player2.y = basePlayerConfig.y;
      player2.height = basePlayerConfig.height;
      highscore = 0;
    }

    if (collison) {
      player.dx = player2.dx = 0;
      player.dy = player2.dy = 0;
      obstacles.forEach((obstacle) => (obstacle.dy = 0));
      return;
    }

    if (player.x > width) {
      player.x = -player.width;
    }

    if (player2.x < 0) {
      player2.x = width;
    }

    obstacles.forEach((obstacle) => moveY(obstacle));

    grow(player, dt * RANDOM_GROW_FACTOR_1);
    grow(player2, dt * RANDOM_GROW_FACTOR_2);

    speed(player, dt * RANDOM_GROW_FACTOR_1);
    speed(player2, dt * RANDOM_GROW_FACTOR_2, true);

    interact(player, pressed('up'), pressed('down'));
    interact(player2, pressed('w'), pressed('s'));

    highscore += dt * 10;
  },
  render: () => {
    player.render();
    player2.render();
    obstacles.forEach((obstacle) => obstacle.render());

    const canvas = document.getElementById('c');
    const ctx = canvas.getContext('2d');
    ctx.font = '30px Arial';
    ctx.fillText(`Highscore: ${Math.floor(highscore) * 100}`, 10, 50);
  },
});

game.start();
