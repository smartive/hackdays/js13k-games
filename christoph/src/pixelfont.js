const letters = {
  'A': [
    [, 1],
    [1, , 1],
    [1, , 1],
    [1, 1, 1],
    [1, , 1]
  ],
  'B': [
    [1, 1],
    [1, , 1],
    [1, 1, 1],
    [1, , 1],
    [1, 1]
  ],
  'C': [
    [1, 1, 1],
    [1],
    [1],
    [1],
    [1, 1, 1]
  ],
  'D': [
    [1, 1],
    [1, , 1],
    [1, , 1],
    [1, , 1],
    [1, 1]
  ],
  'E': [
    [1, 1, 1],
    [1],
    [1, 1, 1],
    [1],
    [1, 1, 1]
  ],
  'G': [
    [, 1, 1],
    [1],
    [1, , 1, 1],
    [1, , , 1],
    [, 1, 1]
  ],
  'I': [
    [1, 1, 1],
    [, 1],
    [, 1],
    [, 1],
    [1, 1, 1]
  ],
  'J': [
    [1, 1, 1],
    [, , 1],
    [, , 1],
    [1, , 1],
    [1, 1, 1]
  ],
  'K': [
    [1, , , 1],
    [1, , 1],
    [1, 1],
    [1, , 1],
    [1, , , 1]
  ],
  'N': [
    [1, , , 1],
    [1, 1, , 1],
    [1, , 1, 1],
    [1, , , 1],
    [1, , , 1]
  ],
  'O': [
    [1, 1, 1],
    [1, , 1],
    [1, , 1],
    [1, , 1],
    [1, 1, 1]
  ],
  'R': [
    [1, 1],
    [1, , 1],
    [1, , 1],
    [1, 1],
    [1, , 1]
  ],
  'S': [
    [1, 1, 1],
    [1],
    [1, 1, 1],
    [, , 1],
    [1, 1, 1]
  ],
  'T': [
    [1, 1, 1],
    [, 1],
    [, 1],
    [, 1],
    [, 1]
  ],
  'U': [
    [1, , 1],
    [1, , 1],
    [1, , 1],
    [1, , 1],
    [1, 1, 1]
  ],
  'Y': [
    [1, , 1],
    [1, , 1],
    [, 1],
    [, 1],
    [, 1]
  ],
  '1': [
    [, 1],
    [, 1],
    [, 1],
    [, 1],
    [, 1]
  ],
  '3': [
    [1, 1, 1],
    [0, 0, 1],
    [1, 1, 1],
    [0, 0, 1],
    [1, 1, 1]
  ],
  '>': [
    [1, 0, 0],
    [0, 1, 0],
    [1, 1, 1],
    [0, 1, 0],
    [1, 0, 0]
  ],
  '?': [
    [1, 1, 1],
    [0, 0, 1],
    [0, 1, 0],
    [0, 0, 0],
    [0, 1, 0]
  ],
  ' ': [
    [, ,],
    [, ,],
    [, ,],
    [, ,],
    [, ,]
  ],
};

/**
 * write in pixel font
 *
 * @export
 * @param {string} text
 * @param {string} color
 * @param {number} size
 * @param {number} x
 * @param {number} y
 */
export function writeString(text, color, size, x, y) {
  const needed = [];
  text = text.toUpperCase();
  for (let i = 0; i < text.length; i++) {
    const letter = letters[text.charAt(i)];
    if (letter) {
      needed.push(letter);
    }
  }

  kctx.save();
  kctx.translate(x, y);
  kctx.fillStyle = color;
  let currX = 0;
  for (i = 0; i < needed.length; i++) {
    const letter = needed[i];
    let currY = 0;
    let addX = 0;
    for (let y = 0; y < letter.length; y++) {
      const row = letter[y];
      for (let x = 0; x < row.length; x++) {
        if (row[x]) {
          kctx.fillRect(currX + x * size, currY, size, size);
        }
      }
      addX = Math.max(addX, row.length * size);
      currY += size;
    }
    currX += size + addX;
  }
  kctx.restore();
}
