import { Level, levelData } from './elements/level';
import { Menu, WinScreen } from './menu';
import { startSound } from './sound';

export class Sokoban {
  constructor() {
    this.levelNumber = 1;

    this.menu = Menu;
    this.winScreen = WinScreen;

    /**
     * @type {Level}
     */
    this.level = null;

    this.winScreen.replay = () => {
      this.levelNumber = 1;
      this.level = null;
      this.winScreen.hide();
      this.menu.show();
    };
  }

  start() {
    startSound();
    this.loop = k.gameLoop({
      update: (dt) => this.update(dt),
      render: () => this.render(),
    });

    this.loop.start();

    kkb('r', () => this.level && this.level.reset());
    kkb('m', () => {
      this.levelNumber = 1;
      this.level = null;
      this.menu.show();
    });

    this.menu.show();
  }

  update(dt) {
    if (this.winScreen.shown) {
      this.winScreen.update(dt);
      return;
    }
    if (this.menu.shown) {
      this.menu.update(dt);
      return;
    }

    if (!this.level) {
      this.level = new Level(this.levelNumber++);
    }

    this.level.update(dt);
    if (!this.level.won) {
      return;
    }

    if (this.levelNumber < Object.keys(levelData).length) {
      this.level = null;
      return;
    }

    this.winScreen.show();
  }

  render() {
    if (this.winScreen.shown) {
      this.winScreen.render();
      return;
    }
    if (this.menu.shown) {
      this.menu.render();
      return;
    }
    if (this.level) {
      this.level.render();
    }
  }
}
