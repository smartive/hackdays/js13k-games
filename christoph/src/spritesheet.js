export const Spritesheet = k.spriteSheet({
  image: imgs[require('./assets/sprites.png')],
  frameWidth: 32,
  frameHeight: 32,
  animations: {
    playerIdle: {
      frames: 0,
    },
    playerWalk: {
      frames: '1..2',
      frameRate: 5,
    },
    goal: {
      frames: 4,
    },
    floor: {
      frames: 5,
    },
    wall: {
      frames: 6,
    },
    box: {
      frames: 3,
    },
    boxOnGoal: {
      frames: 7,
    },
  },
});
