import 'kontra/dist/core';

import './styles/main.css';
import 'kontra/dist/assets';
import 'kontra/dist/gameLoop';
import 'kontra/dist/keyboard';
import 'kontra/dist/sprite';
import 'kontra/dist/spriteSheet';

window.k = kontra;

k.init();
k
  .assets
  .load(require('./assets/sprites.png'))
  .then(() => {
    require('./optimizations');
    const { Sokoban } = require('./game');
    new Sokoban().start();
  });
