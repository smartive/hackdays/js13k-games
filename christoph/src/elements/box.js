import { Spritesheet } from '../spritesheet';

/**
 * Create a box
 *
 * @export
 * @param {number} x
 * @param {number} y
 * @param {Sprite[]} [elements]
 */
export function createBox(x, y, elements = []) {
  const size = 32;
  const velocity = 1.5;

  const box = ks({
    x: x * size,
    y: y * size,

    width: size,
    height: size,

    type: 'box',
    animations: Spritesheet.animations,

    moving: false,
    target: {
      x: 0,
      y: 0,
    },

    canMove(targetX, targetY) {
      const targetSprite = {
        x: this.x + targetX * size,
        y: this.y + targetY * size,
        width: size,
        height: size,
      };
      // The box can move if there are no other
      // objects (no walls or other boxes)
      const collidables = elements.filter(e => e.type === 'box' || e.type === 'wall');
      return !collidables.some(c => c.collidesWith(targetSprite));
    },

    moveBox(targetX, targetY) {
      this.dx = targetX * velocity;
      this.dy = targetY * velocity;
      this.target = {
        x: this.x + targetX * size,
        y: this.y + targetY * size,
      };
      this.moving = true;
    },

    hasHitTarget() {
      return (this.dx > 0 && this.x >= this.target.x) ||
        (this.dx < 0 && this.x <= this.target.x) ||
        (this.dy > 0 && this.y >= this.target.y) ||
        (this.dy < 0 && this.y <= this.target.y);
    },

    clearTarget() {
      this.moving = false;
      this.dx = this.dy = 0;
      this.x = this.target.x;
      this.y = this.target.y;
    },

    update() {
      this.advance();

      if (!this.moving) {
        const onGoal = elements.filter(e => e.type === 'goal').some(e => this.collidesWith(e));
        if (onGoal && this._ca !== this.animations.boxOnGoal) {
          this.playAnimation('boxOnGoal');
        } else if (!onGoal && this._ca !== this.animations.box) {
          this.playAnimation('box');
        }
        return;
      }

      this.hasHitTarget() && this.clearTarget();
    },

  });
  box.playAnimation('box');
  return box;
}
