import { Spritesheet } from '../spritesheet';

function radians(degrees) {
  return degrees * Math.PI / 180;
}

const rotations = {
  0: {
    x: 0,
    y: 0,
    rad: radians(0),
  },
  1: {
    x: 32,
    y: 0,
    rad: radians(90),
  },
  2: {
    x: 0,
    y: 32,
    rad: radians(-90),
  },
  3: {
    x: 32,
    y: 32,
    rad: radians(180),
  },
}

/**
 * Create new player sprite instance
 *
 * @export
 * @param {number} x
 * @param {number} y
 * @param {Sprite[]} [elements]
 * @returns
 */
export function createPlayer(x, y, elements = []) {
  const size = 32;

  const player = ks({
    x: x * size,
    y: y * size,

    width: size,
    height: size,
    animations: Spritesheet.animations,

    type: 'player',
    rotation: rotations[0],

    moving: false,
    target: {
      x: 0,
      y: 0,
    },

    canMove(targetX, targetY) {
      const targetSprite = {
        x: this.x + targetX * size,
        y: this.y + targetY * size,
        width: size,
        height: size,
      };
      const collidables = {
        walls: elements.filter(e => e.type === 'wall'),
        boxes: elements.filter(e => e.type === 'box'),
      }
      // Does the target position (sprite) hit a wall?
      if (collidables.walls.some(wall => wall.collidesWith(targetSprite))) {
        return false;
      }
      // Does the target position hit a box?
      // if yes: check if the box can move (aka has empty space)
      const possibleBox = collidables.boxes.find(box => box.collidesWith(targetSprite));
      if (possibleBox) {
        const canMoveBox = possibleBox.canMove(targetX, targetY);
        if (canMoveBox) {
          possibleBox.moveBox(targetX, targetY);
        }
        return canMoveBox;
      }
      // Return true otherwise (theres nothing in the way)
      return true;
    },

    getInputs() {
      return [
        {
          pressed: kPr('s'),
          rotation: rotations[0],
          dx: 0,
          dy: 1.5,
        },
        {
          pressed: kPr('a'),
          rotation: rotations[1],
          dx: -1.5,
          dy: 0,
        },
        {
          pressed: kPr('d'),
          rotation: rotations[2],
          dx: 1.5,
          dy: 0,
        },
        {
          pressed: kPr('w'),
          rotation: rotations[3],
          dx: 0,
          dy: -1.5,
        },
      ];
    },

    hasHitTarget() {
      return (this.dx > 0 && this.x >= this.target.x) ||
        (this.dx < 0 && this.x <= this.target.x) ||
        (this.dy > 0 && this.y >= this.target.y) ||
        (this.dy < 0 && this.y <= this.target.y);
    },

    setTarget(pressedInput) {
      if (!pressedInput) {
        return;
      }
      this.rotation = pressedInput.rotation;
      const moveX = Math.min(Math.max(pressedInput.dx, -1), 1);
      const moveY = Math.min(Math.max(pressedInput.dy, -1), 1);
      if (!this.canMove(moveX, moveY)) {
        return;
      }
      this.dx = pressedInput.dx;
      this.dy = pressedInput.dy;
      this.target = {
        x: this.x + moveX * size,
        y: this.y + moveY * size,
      };
      this.moving = true;
    },

    clearTarget() {
      this.moving = false;
      this.dx = this.dy = 0;
      this.x = this.target.x;
      this.y = this.target.y;
    },

    update() {
      this.advance();

      if (this.moving) {
        this.hasHitTarget() && this.clearTarget();
        return;
      }

      const inputs = this.getInputs();

      const pressedInput = inputs.find(input => input.pressed);
      const walking = !!pressedInput;

      this.setTarget(pressedInput);

      if (walking && this._ca !== this.animations.playerWalk) {
        this.playAnimation('playerWalk');
      } else if (!walking && this._ca !== this.animations.playerIdle) {
        this.playAnimation('playerIdle');
      }
    },

    render() {
      kctx.save();
      kctx.translate(this.x + this.rotation.x, this.y + this.rotation.y);
      kctx.rotate(this.rotation.rad);
      this._ca.render({ x: 0, y: 0 });
      kctx.restore();
    },
  });
  player.playAnimation('playerIdle');
  return player;
}
