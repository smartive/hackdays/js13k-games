import { Spritesheet } from '../spritesheet';

/**
 * Create a floor
 *
 * @export
 * @param {number} x
 * @param {number} y
 */
export function createFloor(x, y) {
  const size = 32;
  const floor = ks({
    x: x * size,
    y: y * size,

    width: size,
    height: size,
    animations: Spritesheet.animations,

    type: 'floor',
  });
  floor.playAnimation('floor');
  return floor;
}
