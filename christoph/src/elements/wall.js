import { Spritesheet } from '../spritesheet';

/**
 * Create a wall
 *
 * @export
 * @param {number} x
 * @param {number} y
 */
export function createWall(x, y) {
  const size = 32;
  const wall = ks({
    x: x * size,
    y: y * size,

    width: size,
    height: size,
    animations: Spritesheet.animations,

    type: 'wall',
  });
  wall.playAnimation('wall');
  return wall;
}
