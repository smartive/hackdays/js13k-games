import { Spritesheet } from '../spritesheet';

/**
 * Create a goal
 *
 * @export
 * @param {number} x
 * @param {number} y
 */
export function createGoal(x, y) {
  const size = 32;
  const goal = ks({
    x: x * size,
    y: y * size,

    width: size,
    height: size,
    animations: Spritesheet.animations,

    type: 'goal',
  });
  goal.playAnimation('goal');
  return goal;
}
