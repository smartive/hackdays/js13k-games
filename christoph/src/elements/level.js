import { createBox } from './box';
import { createFloor } from './floor';
import { createGoal } from './goal';
import { createPlayer } from './player';
import { createWall } from './wall';

/**
 * @type {{[key: number]: string[]}}
 */
export const levelData = {
  1: [
    '###########',
    '#.*o@o.*..#',
    '###########',
  ],
  2: [
    '########',
    '#.**#..#',
    '#.**#.o###',
    '#..##....#',
    '##.o...o.#',
    ' #.##..###',
    ' #...o##',
    ' ###..#',
    '   #@.#',
    '   ####',
  ],
  3: [
    " #######",
    "##.....##",
    "#..*.o.@#",
    "#.o$oo*.#",
    "#.o**o*.#",
    "#.o**o*.#",
    "#########",
  ],
  4: [
    "#######",
    "#.o*..#",
    "#.$...#",
    "#.$...#",
    "#.$$$.#",
    "#...$.#",
    "##..$.#",
    " ##.$.#",
    "  #.$.#",
    "  ##.@#",
    "   ####",
  ],
  5: [
    "   #####",
    "   #...##",
    "   #@o..##",
    "   #ooo..#",
    "   #.o.o.#",
    "   #.oo..#",
    "   #.o..##",
    "   #.####",
    "   #.**#",
    "   #.**#",
    "   #.*##",
    "####.**#",
    "#....**#",
    "#.#.o*##",
    "#...####",
    "#####",
  ],
};

/**
 * loop lvel data
 *
 * @param {string[]} data
 * @param {(x:number, y:number, spot:string) => void} callback
 */
function loopLevelData(data, callback) {
  for (let y = 0; y < data.length; y++) {
    const line = data[y];
    for (let x = 0; x < line.length; x++) {
      callback(x, y, line[x]);
    }
  }
}

export class Level {
  /**
   *Creates an instance of Level.
   * @param {number} levelNumber
   * @memberof Level
   */
  constructor(levelNumber) {
    this.levelNumber = levelNumber;
    this.reset();
  }

  get goalCount() {
    return this.goals.length;
  }

  get won() {
    return this.goalCount === this.reachedGoals && !this.boxes.some(b => b.moving);
  }

  reset() {
    this.elements = [];

    this.boxes = [];
    this.goals = [];
    this.reachedGoals = 0;

    this.createAssets();
  }

  createAssets() {
    const data = levelData[this.levelNumber];

    const translateY = (kctx.canvas.height / 2 / 32) - (data.length / 2);
    const translateX = (kctx.canvas.width / 2 / 32) - (data.reduce((max, cur) => Math.max(max, cur.length), 0) / 2);

    loopLevelData(
      data,
      (x, y, spot) => {
        if (spot !== ' ') {
          this.elements.push(createFloor(x + translateX, y + translateY));
        }
      },
    );

    loopLevelData(
      data,
      (x, y, spot) => {
        switch (spot) {
          case '#':
            this.elements.push(createWall(x + translateX, y + translateY));
            break;
          case '*':
          case '$':
            const goal = createGoal(x + translateX, y + translateY);
            this.elements.push(goal);
            this.goals.push(goal);
            break;
        }
      },
    );

    loopLevelData(
      data,
      (x, y, spot) => {
        switch (spot) {
          case '@':
            this.elements.push(createPlayer(x + translateX, y + translateY, this.elements));
            break;
          case 'o':
          case '$':
            const box = createBox(x + translateX, y + translateY, this.elements);
            this.elements.push(box);
            this.boxes.push(box);
            break;
        }
      },
    );
  }

  checkWin() {
    const colliding = this.goals.filter(g => this.boxes.some(b => b.collidesWith(g))).length;
    if (this.reachedGoals === colliding) {
      return;
    }
    this.reachedGoals = colliding;
  }

  update(dt) {
    if (this.won) {
      return;
    }
    for (const element of this.elements) {
      element.update(dt);
    }

    this.checkWin();
  }

  render() {
    for (const element of this.elements) {
      element.render();
    }

    kctx.save();

    kctx.font = '20px sans-serif';
    kctx.fillStyle = '#eee';

    kctx.fillText(`Level ${this.levelNumber}`, 10, 30);

    kctx.font = '16px sans-serif';
    kctx.fillText('Press "r" to reset.', 10, 480);
    kctx.fillText('Press "m" to go back to the menu.', 10, 500);

    kctx.font = '20px sans-serif';
    kctx.textAlign = 'right';
    kctx.fillText(`Reached ${this.reachedGoals} of ${this.goalCount} goals`, 500, 30);

    kctx.restore();
  }
}
