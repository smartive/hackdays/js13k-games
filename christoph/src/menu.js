import { writeString } from './pixelfont';

export const Menu = ks({
  x: 0,
  y: 0,
  width: 512,
  height: 512,
  color: '#111',

  shown: false,
  buttons: [],
  selectedButton: 0,

  show() {
    this.shown = true;
    this.buttons = [
      {
        text: 'start',
        select: () => this.hide(),
      },
      {
        text: 'credits',
        select: () => window.location.href = 'https://smartive.ch',
      },
      {
        text: 'back',
        select: () => window.history.back(),
      },
    ];

    kkb('s', () => {
      this.selectedButton++;
      if (this.selectedButton > (this.buttons.length - 1)) {
        this.selectedButton = 0;
      }
    });
    kkb('w', () => {
      this.selectedButton--;
      if (this.selectedButton < 0) {
        this.selectedButton = this.buttons.length - 1;
      }
    });
    kkb('e', () => this.buttons[this.selectedButton].select());
  },

  hide() {
    this.shown = false;
    this.buttons = [];
    kku(['w', 's', 'e']);
  },

  update() {
    if (!this.shown) {
      return;
    }

    this.advance();
  },

  render() {
    if (!this.shown) {
      return;
    }

    this.draw();
    writeString('JS13k', 'white', 12, 150, 10);
    writeString('sokoban', '#eee', 10, 120, 120);

    kctx.save();
    kctx.font = '16px sans-serif';
    kctx.fillStyle = '#eee';
    kctx.fillText('Controls:', 10, 460);
    kctx.fillText('Menu: w = up, s = down, e = select', 10, 480);
    kctx.fillText('Game: w,a,s,d, r = reset, m = back to menu', 10, 500);
    kctx.restore();

    const offset = 200;
    for (let idx = 0; idx < this.buttons.length; idx++) {
      writeString(this.selectedButton === idx ? `> ${this.buttons[idx].text}` : this.buttons[idx].text, '#eee', 4, 180, offset + idx * 40);
    }
  }
});

export const WinScreen = ks({
  x: 0,
  y: 0,
  width: 512,
  height: 512,
  color: '#111',

  shown: false,

  replay: undefined,

  show() {
    this.shown = true;
    kkb('e', () => this.replay());
  },

  hide() {
    this.shown = false;
    kku('e');
  },

  update() {
    if (!this.shown) {
      return;
    }

    this.advance();
  },

  render() {
    if (!this.shown) {
      return;
    }

    this.draw();
    writeString('YOU did it', 'white', 12, 35, 100);
    writeString('> again?', '#eee', 4, 180, 250);
  }
});

