const fs = require('fs');
const path = require('path');
const archiver = require('archiver');

let output = fs.createWriteStream(path.join(__dirname, '..', 'out', 'build.zip'));
let archive = archiver('zip', {
    zlib: { level: 9 },
});

output.on('close', () => {
    console.log(`${archive.pointer()} / 13312 total bytes`);
});

archive.on('warning', (err) => {
    if (err.code === 'ENOENT') {
        console.warn(err)
    } else {
        throw err;
    }
});

archive.on('error', (err) => {
    throw err;
});

archive.pipe(output);
archive.append(
    fs.createReadStream(path.join(__dirname, '..', 'build', 'index.html')), {
        name: 'index.html',
    },
);
archive.directory(path.join(__dirname, '..', 'build', 'media'), '/media');

archive.finalize();
