export function componentToHex(c) {
  var hex = Math.floor(c).toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

export function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

const SEED = 1 / Math.random();

export function random(str) {
  const seed = SEED;
  var l = str.length,
    h = seed ^ l,
    i = 0,
    k;

  while (l >= 4) {
    k =
      (str.charCodeAt(i) & 0xff) |
      ((str.charCodeAt(++i) & 0xff) << 8) |
      ((str.charCodeAt(++i) & 0xff) << 16) |
      ((str.charCodeAt(++i) & 0xff) << 24);

    k =
      (k & 0xffff) * 0x5bd1e995 + ((((k >>> 16) * 0x5bd1e995) & 0xffff) << 16);
    k ^= k >>> 24;
    k =
      (k & 0xffff) * 0x5bd1e995 + ((((k >>> 16) * 0x5bd1e995) & 0xffff) << 16);

    h =
      ((h & 0xffff) * 0x5bd1e995 +
        ((((h >>> 16) * 0x5bd1e995) & 0xffff) << 16)) ^
      k;

    l -= 4;
    ++i;
  }

  switch (l) {
    case 3:
      h ^= (str.charCodeAt(i + 2) & 0xff) << 16;
    case 2:
      h ^= (str.charCodeAt(i + 1) & 0xff) << 8;
    case 1:
      h ^= str.charCodeAt(i) & 0xff;
      h =
        (h & 0xffff) * 0x5bd1e995 +
        ((((h >>> 16) * 0x5bd1e995) & 0xffff) << 16);
  }

  h ^= h >>> 13;
  h = (h & 0xffff) * 0x5bd1e995 + ((((h >>> 16) * 0x5bd1e995) & 0xffff) << 16);
  h ^= h >>> 15;

  const res = h >>> 0;
  return res / Math.pow(2, 32);
}

export const length = p => Math.sqrt(p.x * p.x + p.y * p.y);
export const diff = (p1, p2) => ({ x: p2.x - p1.x, y: p2.y - p1.y });
export const distance = (p1, p2) => length(diff(p1, p2));
export const plus = (p1, p2) => ({ x: p1.x + p2.x, y: p1.y + p2.y });
export const minus = (p1, p2) => ({ x: p1.x - p2.x, y: p1.y - p2.y });

export const add = (p, k) => ({ x: p.x + k, y: p.y + k });
export const subtract = (p, k) => ({ x: p.x - k, y: p.y - k });
export const times = (p, k) => ({ x: p.x * k, y: p.y * k });
export const init = v => ({ x: v, y: v });

export const rectIntersect = (r1, r2) =>
  !(
    r1.p1.x > r2.p2.x ||
    r2.p1.x > r1.p2.x ||
    r1.p1.y > r2.p2.y ||
    r2.p1.y > r1.p2.y
  );

export const bound = (n, max = 1, min = 0) => Math.min(255, Math.max(0, n));
