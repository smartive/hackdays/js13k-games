import "./styles/main.css";
import "kontra/src/core";
import "kontra/src/gameLoop";
import "kontra/src/keyboard";
import {
  rgbToHex,
  random,
  length,
  diff,
  distance,
  minus,
  plus,
  add,
  times,
  rectIntersect,
  subtract,
  bound
} from "./utils";

const sokobanUrl = require("./assets/sokoban.png");
const sokoban = new Image();
sokoban.src = sokobanUrl;

kontra.init();

const WHITE = "#FFFFFF";
const BLACK = "#000000";
const RED = "#FF0000";

const WIDTH = 1000;
const HEIGHT = 1000;
const VIEWPORT = {
  p1: {
    x: 0,
    y: 0
  },
  p2: {
    x: WIDTH,
    y: HEIGHT
  }
};
const QUARTERVP = {
  x: WIDTH / 2,
  y: HEIGHT / 2
};

const UNIVERSE = 1000;
const GALAXY = 10;
const SOLARSYSTEM = 0.1;
const PLANET = 0.01;
const REGION = 0.0001;
const CITY = 0.00001;
const BUILDING = 0.000001;

const CENTER = {
  x: WIDTH / 2,
  y: HEIGHT / 2
};

// real
let pos = {
  x: 500,
  y: 500
};

// perspectival
let speed = {
  x: random("speed-x") - 0.5,
  y: random("speed-y") - 0.5
};

let zoom = 0;

let zoomSpeed = random("speed-z");

let dirty = true;

const ACCELERATION = 0.5;

const ATTRITION = 0.999;

let galaxiesdrawn = 0;
let solarsystemsdrawn = 0;
let planetsdrawn = 0;

let loop = kontra.gameLoop({
  clearCanvas: false,
  fps: 30,
  update: function() {
    if (kontra.keys.pressed("left")) {
      speed = plus(speed, { x: -ACCELERATION, y: 0 });
      dirty = true;
    }
    if (kontra.keys.pressed("right")) {
      speed = plus(speed, { x: ACCELERATION, y: 0 });
      dirty = true;
    }
    if (kontra.keys.pressed("up")) {
      speed = plus(speed, { x: 0, y: -ACCELERATION });
      dirty = true;
    }
    if (kontra.keys.pressed("down")) {
      speed = plus(speed, { x: 0, y: ACCELERATION });
      dirty = true;
    }
    if (kontra.keys.pressed("w")) {
      zoomSpeed++;
      dirty = true;
    }
    if (kontra.keys.pressed("s")) {
      zoomSpeed--;
      dirty = true;
    }

    if (speed.x || spee.y) {
      speed = times(speed, ATTRITION);
      pos = plus(pos, perspectivalToRealVector(speed));
      dirty = true;
    }

    if (zoomSpeed) {
      zoom += zoomSpeed;
      zoomSpeed *= ATTRITION;
      dirty = true;
    }
  },
  render: function() {
    if (dirty) {
      dirty = false;
      galaxiesdrawn = 0;
      solarsystemsdrawn = 0;
      planetsdrawn = 0;

      kontra.context.fillStyle = BLACK;
      kontra.context.fillRect(-WIDTH, -WIDTH, 2 * WIDTH, 2 * HEIGHT);
      for (let i = 0; i < UNIVERSE / GALAXY; i++) {
        for (let j = 0; j < UNIVERSE / GALAXY; j++) {
          drawGalaxy({ x: i, y: j });
        }
      }

      /*
      if (rotation) {
        kontra.context.rotate(rotation / Math.PI / 100000);
      }
      */

      /*
      kontra.context.fillStyle = RED;
      kontra.context.beginPath();
      const centerdraw = realToPerspectival(pos);
      console.log(centerdraw);
      kontra.context.arc(centerdraw.x, centerdraw.y, 100, 0, 2 * Math.PI);
      kontra.context.fill();
      */
      /*
      document.getElementById("info").innerHTML = `
        Galaxies: ${galaxiesdrawn}\n
        Solar systems: ${solarsystemsdrawn}\n
        Planets: ${planetsdrawn}\n
        Zoom: ${zoom}\n
        Relzoom: ${relZoom()}\n
      `;*/
    }
  }
});

loop.start();

const relZoom = () => Math.pow(1.0001, zoom);

const realToPerspectivalVector = p => times(p, relZoom());
const realToPerspectival = p =>
  plus(times(minus(p, pos), relZoom()), QUARTERVP); // (pos + p) * rz + qvp
const realToPerspectivalRect = r => ({
  p1: realToPerspectival(r.p1),
  p2: realToPerspectival(r.p2)
});

const perspectivalToRealRect = r => ({
  p1: perspectivalToReal(r.p1),
  p2: perspectivalToReal(r.p2)
});
const perspectivalToRealVector = p => times(p, 1 / relZoom());
const perspectivalToReal = p =>
  plus(times(minus(p, QUARTERVP), 1 / relZoom()), pos);

function drawGalaxy(galaxy) {
  const viewport = perspectivalToRealRect(VIEWPORT);
  const geocenter = galaxyGeoCenter(galaxy);
  const qrect = {
    p1: subtract(geocenter, GALAXY),
    p2: add(geocenter, 2 * GALAXY)
  };

  if (!rectIntersect(viewport, qrect)) {
    return;
  }
  const relzoom = relZoom();

  const dist = galaxyDist(galaxy);
  const isgalaxy =
    random(`galaxy-${galaxy.x}-${galaxy.y}`) < Math.pow(1 - dist, 10);
  if (isgalaxy) {
    galaxiesdrawn++;
    const color = galaxyColor(galaxy);
    const col = rgbToHex(255, 255, 255 * color);
    const center = galaxyCenter(galaxy);
    kontra.context.fillStyle = col;
    const r = galaxyRadius(galaxy);
    const rotation = galaxyRotation(galaxy);
    kontra.context.beginPath();

    const pgalaxycenter = realToPerspectival(center);

    if (relzoom < 50) {
      kontra.context.ellipse(
        pgalaxycenter.x,
        pgalaxycenter.y,
        r.x * relzoom,
        r.y * relzoom,
        rotation,
        0,
        2 * Math.PI
      );
      kontra.context.fill();
    } else {
      for (let i = 0; i < GALAXY / SOLARSYSTEM; i++) {
        for (let j = 0; j < GALAXY / SOLARSYSTEM; j++) {
          drawSolarSystem(galaxy, { x: i, y: j });
        }
      }
    }
  }
}

function drawSolarSystem(galaxy, solarsystem) {
  const viewport = perspectivalToRealRect(VIEWPORT);
  const galaxycenter = galaxyCenter(galaxy);
  const geocenter = solarSystemGeoCenter(galaxy, solarsystem);

  const rect = {
    p1: subtract(geocenter, SOLARSYSTEM),
    p2: add(geocenter, 2 * SOLARSYSTEM)
  };

  if (!rectIntersect(viewport, rect)) {
    return;
  }

  const relzoom = relZoom();

  const dist = solarSystemDist(galaxy, solarsystem);

  const issolarsystem =
    random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }`
    ) < Math.pow(1 - dist, 20);

  if (issolarsystem) {
    solarsystemsdrawn++;
    const center = solarSystemCenter(galaxy, solarsystem);

    const r = solarSystemRadius(galaxy, solarsystem);

    const pcenter = realToPerspectival(center);

    const color = solarSystemColor(galaxy, solarsystem);
    const col = rgbToHex(255, 255, 255 * color);
    kontra.context.fillStyle = col;

    if (relzoom < 5000) {
      kontra.context.beginPath();
      kontra.context.ellipse(
        pcenter.x,
        pcenter.y,
        r * relzoom,
        r * relzoom,
        0,
        0,
        2 * Math.PI
      );
      kontra.context.fill();
    } else {
      kontra.context.beginPath();
      kontra.context.ellipse(
        pcenter.x,
        pcenter.y,
        r * 0.5 * relzoom,
        r * 0.5 * relzoom,
        0,
        0,
        2 * Math.PI
      );
      kontra.context.fill();
      for (let i = 0; i < SOLARSYSTEM / PLANET; i++) {
        for (let j = 0; j < SOLARSYSTEM / PLANET; j++) {
          drawPlanet(galaxy, solarsystem, { x: i, y: j });
        }
      }
    }
  }
}

function drawPlanet(galaxy, solarsystem, planet) {
  const geocenter = planetGeoCenter(galaxy, solarsystem, planet);
  const rect = {
    p1: subtract(geocenter, PLANET),
    p2: add(geocenter, 2 * PLANET)
  };
  if (!rectIntersect(perspectivalToRealRect(VIEWPORT), rect)) {
    return;
  }

  const isplanet =
    random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }-planet-${planet.x}-${planet.y}`
    ) < 0.1;

  if (isplanet) {
    planetsdrawn++;
    const color = planetColor(galaxy, solarsystem, planet);

    const center = geocenter;

    const pcenter = realToPerspectival(center);

    const r = planetRadius(galaxy, solarsystem, planet);

    const relzoom = relZoom();

    if (relzoom < 30000) {
      const col = rgbToHex(color.r, color.g, color.b);
      kontra.context.fillStyle = col;
      kontra.context.beginPath();
      kontra.context.ellipse(
        pcenter.x,
        pcenter.y,
        r * relzoom,
        r * relzoom,
        0,
        0,
        2 * Math.PI
      );
      kontra.context.fill();
    } else {
      for (let i = 0; i < PLANET / REGION; i++) {
        for (let j = 0; j < PLANET / REGION; j++) {
          drawRegion(galaxy, solarsystem, planet, { x: i, y: j });
        }
      }
    }
  }
}

function drawRegion(galaxy, solarsystem, planet, region) {
  const planetgeocenter = planetGeoCenter(galaxy, solarsystem, planet, region);
  const geocenter = regionGeoCenter(galaxy, solarsystem, planet, region);

  const rect = {
    p1: subtract(geocenter, REGION / 2),
    p2: add(geocenter, REGION / 2)
  };

  if (!rectIntersect(perspectivalToRealRect(VIEWPORT), rect)) {
    return;
  }

  const planetradius = planetRadius(galaxy, solarsystem, planet);
  const regiondist = regionDist(galaxy, solarsystem, planet, region);

  if (regiondist < planetradius) {
    const planetcolor = planetColor(galaxy, solarsystem, planet);
    const colord = regionColor(galaxy, solarsystem, planet, region);
    const pole = Math.abs(geocenter.y - planetgeocenter.y) / planetradius;
    const POLESTART = 0.7;
    const color = {
      r: bound(
        planetcolor.r +
          colord.r +
          (pole > 0.8 ? ((pole - POLESTART) / (1 - POLESTART)) * 128 : 0),
        255
      ),
      g: bound(
        planetcolor.g +
          colord.g +
          (pole > 0.8 ? ((pole - POLESTART) / (1 - POLESTART)) * 128 : 0),
        255
      ),
      b: bound(
        planetcolor.b +
          colord.b +
          (pole > 0.8 ? ((pole - POLESTART) / (1 - POLESTART)) * 128 : 0),
        255
      )
    };

    kontra.context.strokeStyle = rgbToHex(color.r, color.g, color.b);
    kontra.context.fillStyle = rgbToHex(color.r, color.g, color.b);
    const prect = realToPerspectivalRect(rect);
    kontra.context.fillRect(
      prect.p1.x,
      prect.p1.y,
      prect.p2.x - prect.p1.x,
      prect.p2.y - prect.p1.y
    );

    const relzoom = relZoom();

    if (relzoom >= 4000000) {
      for (let i = 0; i < REGION / CITY; i++) {
        for (let j = 0; j < REGION / CITY; j++) {
          drawCity(galaxy, solarsystem, planet, region, { x: i, y: j });
        }
      }
    }
  }
}

function drawCity(galaxy, solarsystem, planet, region, city) {
  const geocenter = cityGeoCenter(galaxy, solarsystem, planet, region, city);
  const rect = {
    p1: subtract(geocenter, CITY / 2),
    p2: add(geocenter, CITY / 2)
  };

  if (!rectIntersect(perspectivalToRealRect(VIEWPORT), rect)) {
    return;
  }

  const iscity =
    random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }-planet-${planet.x}-${planet.y}-region-${region.x}-${region.y}-city-${
        city.x
      }-${city.y}`
    ) < 0.1;

  if (iscity) {
    kontra.context.strokeStyle = rgbToHex(128, 128, 128);
    kontra.context.fillStyle = rgbToHex(128, 128, 128);
    const prect = realToPerspectivalRect(rect);
    kontra.context.fillRect(
      prect.p1.x,
      prect.p1.y,
      prect.p2.x - prect.p1.x,
      prect.p2.y - prect.p1.y
    );

    if (relZoom() >= 30000000) {
      for (let i = 0; i < CITY / BUILDING; i++) {
        for (let j = 0; j < CITY / BUILDING; j++) {
          drawBuilding(galaxy, solarsystem, planet, region, city, {
            x: i,
            y: j
          });
        }
      }
    }
  }
}

function drawBuilding(galaxy, solarsystem, planet, region, city, building) {
  const geocenter = buildingGeoCenter(
    galaxy,
    solarsystem,
    planet,
    region,
    city,
    building
  );
  const rect = {
    p1: subtract(geocenter, BUILDING / 2),
    p2: add(geocenter, BUILDING / 2)
  };

  if (!rectIntersect(perspectivalToRealRect(VIEWPORT), rect)) {
    return;
  }

  const cat = random(
    `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
      solarsystem.y
    }-planet-${planet.x}-${planet.y}-region-${region.x}-${region.y}-city-${
      city.x
    }-${city.y}-building-${building.x}-${building.y}`
  );

  const prect = realToPerspectivalRect(rect);
  if (cat < 0.1) {
    kontra.context.drawImage(
      sokoban,
      prect.p1.x,
      prect.p1.y,
      prect.p2.x - prect.p1.x,
      prect.p2.y - prect.p1.y
    );
  }
}

const drawRect = r => {
  const pr = realToPerspectivalRect(r);
  kontra.context.strokeStyle = RED;
  kontra.context.rect(pr.p1.x, pr.p1.y, pr.p2.x - pr.p1.x, pr.p2.y - pr.p1.y);
  kontra.context.stroke();
};

const galaxyDist = galaxy =>
  distance(galaxyGeoCenter(galaxy), CENTER) /
  Math.sqrt(2 * UNIVERSE * UNIVERSE);

const galaxyGeoCenter = galaxy => times(add(galaxy, 0.5), GALAXY);

const galaxyCenter = galaxy =>
  plus(galaxyGeoCenter(galaxy), {
    x: (random(`galaxy-${galaxy.x}-${galaxy.y}-center-x`) * GALAXY) / 2,
    y: (random(`galaxy-${galaxy.x}-${galaxy.y}-center-y`) * GALAXY) / 2
  });

const galaxyColor = galaxy => random(`galaxy-${galaxy.x}-${galaxy.y}-color`);

const galaxyRadius = galaxy => ({
  x:
    ((bound(random(`galaxy-${galaxy.x}-${galaxy.y}-r.x`), 1, 0.1) * GALAXY) /
      2) *
    (1 - galaxyDist(galaxy)) *
    0.5,
  y:
    ((bound(random(`galaxy-${galaxy.x}-${galaxy.y}-r.y`), 1, 0.1) * GALAXY) /
      2) *
    (1 - galaxyDist(galaxy)) *
    0.5
});

const galaxyRotation = galaxy =>
  random(`galaxy-${galaxy.x}-${galaxy.y}-rotation`) * 2 * Math.PI;

const solarSystemGeoCenter = (galaxy, solarsystem) =>
  plus(galaxyGeoCenter(galaxy), times(add(solarsystem, 0.5), SOLARSYSTEM));

const solarSystemCenter = (galaxy, solarsystem) =>
  plus(solarSystemGeoCenter(galaxy, solarsystem), {
    x:
      (random(
        `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
          solarsystem.y
        }-center-x`
      ) *
        SOLARSYSTEM) /
      4,
    y:
      (random(
        `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
          solarsystem.y
        }-center-y`
      ) *
        SOLARSYSTEM) /
      4
  });

const solarSystemDist = (galaxy, solarsystem) =>
  distance(solarSystemGeoCenter(galaxy, solarsystem), galaxyCenter(galaxy)) /
  Math.sqrt(2 * GALAXY * GALAXY);

const solarSystemRadius = (galaxy, solarsystem) =>
  ((bound(
    random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }-r`
    ),
    1,
    0.1
  ) *
    SOLARSYSTEM) /
    2) *
  (1 - solarSystemDist(galaxy, solarsystem)) *
  0.2;

const solarSystemColor = (galaxy, solarsystem) =>
  bound(
    galaxyColor(galaxy) +
      (random(
        `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
          solarsystem.y
        }-color`
      ) -
        0.5) *
        0.5,
    255
  );

const planetColor = (galaxy, solarsystem, planet) => ({
  r:
    bound(
      random(
        `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
          solarsystem.y
        }-planet-${planet.x}-${planet.y}-color-r`
      )
    ) * 255,
  g:
    bound(
      random(
        `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
          solarsystem.y
        }-planet-${planet.x}-${planet.y}-color-g`
      )
    ) * 255,
  b:
    bound(
      random(
        `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
          solarsystem.y
        }-planet-${planet.x}-${planet.y}-color-b`
      )
    ) * 255
});

const planetGeoCenter = (galaxy, solarsystem, planet) =>
  plus(
    solarSystemGeoCenter(galaxy, solarsystem),
    times(add(planet, 0.5), PLANET)
  );

const planetRadius = (galaxy, solarsystem, planet) =>
  bound(
    random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }-planet-${planet.x}-${planet.y}-r`
    ),
    0.1,
    1
  ) *
  PLANET *
  0.3;

const regionGeoCenter = (galaxy, solarsystem, planet, region) =>
  plus(
    subtract(planetGeoCenter(galaxy, solarsystem, planet), PLANET * 0.5),
    times(add(region, 0.5), REGION)
  );

const regionDist = (galaxy, solarsystem, planet, region) =>
  distance(
    regionGeoCenter(galaxy, solarsystem, planet, region),
    planetGeoCenter(galaxy, solarsystem, planet)
  );

const regionColor = (galaxy, solarsystem, planet, region) => ({
  r:
    (random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }-planet-${planet.x}-${planet.y}-region-${region.x}-${region.y}-color-r`
    ) -
      0.5) *
    255 *
    0.5,
  g:
    (random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }-planet-${planet.x}-${planet.y}-region-${region.x}-${region.y}-color-g`
    ) -
      0.5) *
    255 *
    0.5,
  b:
    (random(
      `galaxy-${galaxy.x}-${galaxy.y}-solarsystem-${solarsystem.x}-${
        solarsystem.y
      }-planet-${planet.x}-${planet.y}-region-${region.x}-${region.y}-color-b`
    ) -
      0.5) *
    255 *
    0.5
});

const cityGeoCenter = (galaxy, solarsystem, planet, region, city) =>
  plus(
    subtract(
      regionGeoCenter(galaxy, solarsystem, planet, region),
      REGION * 0.5
    ),
    times(add(city, 0.5), CITY)
  );

const buildingGeoCenter = (
  galaxy,
  solarsystem,
  planet,
  region,
  city,
  building
) =>
  plus(
    subtract(
      cityGeoCenter(galaxy, solarsystem, planet, region, city),
      CITY * 0.5
    ),
    times(add(building, 0.5), BUILDING)
  );
