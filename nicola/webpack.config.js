const { join } = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const webpack = require('webpack');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = (env) => {
    const devMode = env === 'development';
    return {
        mode: env,
        entry: require.resolve('./src/game.js'),
        output: {
            path: join(__dirname, 'build'),
            publicPath: '/',
        },
        resolve: {
            extensions: ['.js'],
        },
        module: {
            rules: [
                {
                    oneOf: [
                        {
                            test: /\.css$/,
                            use: [
                                devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
                                'css-loader'
                            ]
                        },
                        {
                            loader: require.resolve('file-loader'),
                            exclude: [/\.js$/, /\.html$/, /\.json$/],
                            options: {
                                name: 'media/[name].[ext]',
                            },
                        },
                    ],
                },
            ],
        },
        plugins: [
            new CleanWebpackPlugin(
                [
                    join(__dirname, 'build'),
                ],
            ),
            new MiniCssExtractPlugin({
                filename: '[name].css',
            }),
            devMode ? null : new OptimizeCssAssetsPlugin(),
            new webpack.optimize.ModuleConcatenationPlugin(),
            new HtmlWebpackPlugin({
                title: '',
                template: 'src/index.html',
                minify: devMode === true ? false : {
                    collapseBooleanAttributes: true,
                    collapseInlineTagWhitespace: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeOptionalTags: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    useShortDoctype: true,
                },
                inlineSource: devMode ? '' : /\.(js|css)$/,
            }),
            devMode ? null : new HtmlWebpackInlineSourcePlugin(),
        ].filter(Boolean),
        performance: {
            maxEntrypointSize: 13312,
        },
        devServer: {
            contentBase: join(__dirname, 'build'),
            compress: true,
            port: 8080,
            stats: 'minimal',
        }
    };
}
