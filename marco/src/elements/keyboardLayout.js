export class KeyboardLayout {
    constructor(up = "up", down = "down", left = "left", right = "right") {
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
    }
}
