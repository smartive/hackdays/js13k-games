import { KeyboardLayout } from "./keyboardLayout";

export class Player {
  constructor(color, x, y, dx, dy, keyboardLayout = new KeyboardLayout()) {
    this.spritePaths = [];
    this.playerVelocity = 2;
    this.keyboardLayout = keyboardLayout;
    this.isPlaying = true;
    this.player = kontra.sprite({
      color,
      x,
      y,
      dx,
      dy,
      width: 2,
      height: 2,
      positionHistory: []
    });
    this.addPositionToHistory();
  }

  render() {
    this.player.render();
    this.spritePaths.forEach(sp => sp.render());
  }

  calculatePositionPathDimensions(posA, posB) {
    const x = posA.x < posB.x ? posA.x + 2 : posB.x;
    const y = posA.y < posB.y ? posA.y + 2 : posB.y;

    return {
      x,
      y,
      width: Math.abs(posA.x - posB.x) || 2,
      height: Math.abs(posA.y - posB.y) || 2
    };
  }

  update(dt) {
    kontra.keys.pressed(this.keyboardLayout.left) && this.turnLeft();
    kontra.keys.pressed(this.keyboardLayout.right) && this.turnRight();
    kontra.keys.pressed(this.keyboardLayout.up) && this.turnUp();
    kontra.keys.pressed(this.keyboardLayout.down) && this.turnDown();

    let lastPosition = null;
    let spritePaths = [];
    let spritePath = null;

    this.player.positionHistory.forEach(position => {
      if (lastPosition) {
        spritePath = kontra.sprite({
          color: this.player.color,
          ...this.calculatePositionPathDimensions(position, lastPosition)
        });

        spritePaths.push(spritePath);
      }
      lastPosition = position;
    });

    const position = { x: this.player.position.x, y: this.player.position.y };

    spritePath = kontra.sprite({
      color: this.player.color,
      ...this.calculatePositionPathDimensions(position, lastPosition)
    });

    spritePaths.push(spritePath);
    this.spritePaths = spritePaths;

    if (this.player.position.x >= 512 || this.player.position.x <= 0) {
      this.isPlaying = false;
    }

    if (this.player.position.y >= 512 || this.player.position.y <= 0) {
      this.isPlaying = false;
    }

    this.player.update();
  }

  turnLeft() {
    if (this.player.dx > 0 || this.player.dy !== 0) {
      this.player.dy = 0;
      this.player.dx = -this.playerVelocity;
      this.addPositionToHistory();
    }
  }

  turnRight() {
    if (this.player.dx < 0 || this.player.dy !== 0) {
      this.player.dy = 0;
      this.player.dx = this.playerVelocity;
      this.addPositionToHistory();
    }
  }

  turnDown() {
    if (this.player.dx !== 0 || this.player.dy < 0) {
      this.player.dx = 0;
      this.player.dy = this.playerVelocity;
      this.addPositionToHistory();
    }
  }

  turnUp() {
    if (this.player.dx !== 0 || this.player.dy > 0) {
      this.player.dx = 0;
      this.player.dy = -this.playerVelocity;
      this.addPositionToHistory();
    }
  }

  addPositionToHistory() {
    this.player.positionHistory.push({
      x: this.player.position.x,
      y: this.player.position.y
    });
  }

  checkCollisionsWith(otherPlayerSprite) {
    this.spritePaths.forEach(sp => {
      const lastPosition = this.spritePaths.slice(-1)[0];

      if (sp.collidesWith(otherPlayerSprite)) {
        this.isPlaying = false;
      }
    });
  }
}
