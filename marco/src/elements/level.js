import { Player } from "./player";
import { KeyboardLayout } from "./keyboardLayout";

export class Level {
  constructor() {
    this.winner = null;

    this.player1 = new Player("red", 20, 20, 1, 0);
    this.player2 = new Player("blue", 490, 490, -1, 0, new KeyboardLayout("w", "s", "a", "d"));
    this.player3 = new Player("green", 490, 20, -1, 0, new KeyboardLayout("i", "k", "j", "l"));
    this.players = [this.player1, this.player2, this.player3];
  }

  render() {
    this.players.filter(p => p.isPlaying).forEach(p => p.render());
  }

  update(dt) {
    this.players.forEach(p => {
      const otherPlayers = this.players.filter(currentPlayer => currentPlayer.player.color !== p.player.color);
      const otherPlayersSpritePaths = otherPlayers.reduce((acc, currentPlayer) => {
        acc = [...acc, ...currentPlayer.spritePaths];
        return acc;
      }, []);

      otherPlayersSpritePaths.forEach(otherPlayerSp => p.checkCollisionsWith(otherPlayerSp));
    });

    this.players.forEach(p => p.update(dt));
  }
}
