import { Player } from "./elements/player";
import { Level } from "./elements/level";

export class Tron {
  constructor() {
    /**
     * @type {Level}
     */
    this.level = null;
  }

  start() {
    this.loop = kontra.gameLoop({
      update: dt => this.update(dt),
      render: () => this.render()
    });

    this.loop.start();
  }

  update(dt) {
    if (!this.level) {
      this.level = new Level(this.levelNumber++);
    }

    this.level.update(dt);
  }

  render() {
    if (this.level) {
      this.level.render();
    }
  }
}
