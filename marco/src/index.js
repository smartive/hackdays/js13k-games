import "kontra/src/core";

import "./styles/main.css";
import "kontra/src/assets";
import "kontra/src/gameLoop";
import "kontra/src/keyboard";
import "kontra/src/sprite";
import "kontra/src/spriteSheet";

kontra.init();
const { Tron } = require("./game");
new Tron().start();
