## Bootstrap for js13k!

1. check out
2. `npm install`
3. delete `.git folder` (you wanna have a new repo by yourself)
4. code stuff -> `npm start`
5. ...
6. profit!

Uses webpack for everything.
- `npm start`: start webpack dev mode
- `npm run develop`: create development build (for debug)
- `npm run build`: create production build (inlined)
- `npm run package`: create build and zip the file

TODO for yourself: add assets :)
