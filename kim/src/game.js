import "./styles/main.css";
import "kontra/src/core";
import "kontra/src/sprite";
import "kontra/src/gameLoop";
import "kontra/src/keyboard";

// import { foobar } from "./test";
// foobar();

kontra.init();

const SPEED = 2; // *60px/s
const CANVAS_SIZE = 512;
const PIXELS_1PERCENT = Math.floor((CANVAS_SIZE * CANVAS_SIZE) / 100);
const TÖTZLI_SIZE = 10;
const LINE_SIZE = 2;
const LINE_OFFSET = (TÖTZLI_SIZE - LINE_SIZE) / 2;

let tötzli = kontra.sprite({
  x: 200,
  y: 502,
  color: "black",
  width: TÖTZLI_SIZE,
  height: TÖTZLI_SIZE,
  dx: -SPEED, // to right
  dy: 0 // to bottom
});

let field = kontra.sprite({
  x: 0,
  y: 0,
  width: CANVAS_SIZE,
  height: CANVAS_SIZE,
  color: "#e3e7fa"
});

let line = kontra.sprite({
  x: -99,
  y: -99,
  color: "grey",
  width: LINE_SIZE,
  height: LINE_SIZE,
  drawing: false,
  direction: "none"
});

let loop = kontra.gameLoop({
  update: function() {
    // update the game state
    tötzli.update();
    line.update();

    // ------ line ------
    if (line.drawing) {
      // line completed?
      if (line.width >= field.width || line.height >= field.height) {
        // trim playing field horizontally
        if (line.direction === "right" || line.direction === "left") {
          const threshold = field.height / 2 + field.y;
          if (line.y < threshold) {
            // trim oben
            field.height = field.height - line.y + field.y;
            field.y = line.y;
          } else {
            // trim unten
            field.height = line.y - field.y;
          }
        }

        // trim playing field vertically
        if (line.direction === "up" || line.direction === "down") {
          const threshold = field.width / 2 + field.x;
          if (line.x < threshold) {
            // trim links
            field.width = field.width - line.x + field.x;
            field.x = line.x;
          } else {
            // trim rechts
            field.width = line.x - field.x;
          }
        }

        // won?
        const percentCovered =
          100 - Math.ceil((field.width * field.height) / PIXELS_1PERCENT);
        if (percentCovered > 75) {
          console.log("WIN!");
          loop.stop();
        } else {
          console.log(`You covered ${percentCovered}% – get to 75%!`);
        }

        // reset
        line.x = -99;
        line.y = -99;
        line.width = LINE_SIZE;
        line.height = LINE_SIZE;
        line.drawing = false;
        return;
      }

      // draw line
      switch (line.direction) {
        case "right":
          line.width = line.width + SPEED;
          break;
        case "left":
          line.width = line.width + SPEED;
          line.x = line.x - SPEED;
          break;
        case "down":
          line.height = line.height + SPEED;
          break;
        case "up":
          line.height = line.height + SPEED;
          line.y = line.y - SPEED;
          break;
        default:
          break;
      }
    }

    // ----- tötzli tütscht am rand aa ------
    // touches left side
    if (tötzli.dx < 0 && tötzli.x <= field.x) {
      // console.log("touched left");
      const wallIsTop = tötzli.y <= field.y + field.height;
      tötzli.dy = wallIsTop ? SPEED : -SPEED;
      tötzli.dx = 0;
      return;
    }
    // touches right side
    else if (
      tötzli.dx > 0 &&
      tötzli.x + tötzli.width >= field.width + field.x
    ) {
      // console.log("touched right");
      const wallIsTop = tötzli.y <= field.y;
      tötzli.dy = wallIsTop ? SPEED : -SPEED;
      tötzli.dx = 0;
      return;
    }
    // touches bottom
    else if (
      tötzli.dy > 0 &&
      tötzli.y + tötzli.height >= field.height + field.y
    ) {
      // console.log("touched bottom");
      const wallIsLeft = tötzli.x <= field.x;
      tötzli.dx = wallIsLeft ? SPEED : -SPEED;
      tötzli.dy = 0;
      return;
    }
    // touches top
    else if (tötzli.dy < 0 && tötzli.y <= field.y) {
      // console.log("touched top");
      const wallIsLeft = tötzli.x <= field.x + field.width;
      tötzli.dx = wallIsLeft ? SPEED : -SPEED;
      tötzli.dy = 0;
      return;
    }

    // ------ key press (-> tötzli ins feld + linie) ------
    if (
      kontra.keys.pressed("right") &&
      tötzli.dx === 0 && // moving horizontally
      tötzli.x <= field.x // am linken rand
    ) {
      // nach rechts
      // console.log("right");
      tötzli.dx = Math.abs(tötzli.dy);
      tötzli.dy = 0;

      line.width = TÖTZLI_SIZE;
      line.x = tötzli.x;
      line.y = tötzli.y + LINE_OFFSET;
      line.drawing = true;
      line.direction = "right";
    } else if (
      kontra.keys.pressed("left") &&
      tötzli.dx === 0 && // moving horizontally
      tötzli.x >= field.x + field.width - TÖTZLI_SIZE // am rechten rand
    ) {
      // nach links
      // console.log("left");
      tötzli.dx = -Math.abs(tötzli.dy);
      tötzli.dy = 0;

      line.width = TÖTZLI_SIZE;
      line.x = tötzli.x;
      line.y = tötzli.y + LINE_OFFSET;
      line.drawing = true;
      line.direction = "left";
    }
    if (
      kontra.keys.pressed("down") &&
      tötzli.dy === 0 && // moving vertically
      tötzli.y <= field.y // oben
    ) {
      // nach unten
      // console.log("down");
      tötzli.dy = Math.abs(tötzli.dx);
      tötzli.dx = 0;

      line.height = TÖTZLI_SIZE;
      line.x = tötzli.x + LINE_OFFSET;
      line.y = tötzli.y;
      line.drawing = true;
      line.direction = "down";
    } else if (
      kontra.keys.pressed("up") &&
      tötzli.dy === 0 && // moving vertically
      tötzli.y >= field.y + field.height - TÖTZLI_SIZE // unten
    ) {
      // nach oben
      // console.log("up");
      tötzli.dy = -Math.abs(tötzli.dx);
      tötzli.dx = 0;

      line.height = TÖTZLI_SIZE;
      line.x = tötzli.x + LINE_OFFSET;
      line.y = tötzli.y;
      line.drawing = true;
      line.direction = "up";
    }
  },
  render: function() {
    field.render();
    line.render();
    tötzli.render();
  }
});

loop.start(); // start the game
